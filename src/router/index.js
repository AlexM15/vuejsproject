import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Accueil",
    component: () => import("../views/Accueil.vue")
  },
  {
    path: "/ajouter",
    name: "Ajout film",
    component: () => import("../views/AjoutMovie.vue")
  },
  {
    path: "/movie/:id",
    name: "Details film",
    component: () => import("../views/MovieDetails.vue")
  },
  {
    path: "/movie/:id/edit",
    name: "Edit de film",
    component: () => import("../views/EditMovie.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
