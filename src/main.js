import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

window.shared_data = {
  movies: [
    {
      title: "Jumanji : Bienvenue dans la jungle",
      anneePublication: "2017",
      langue: "Anglais",
      urlPoster:
        "https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg",
      realisateur: {
        nom: "Kasdan",
        prenom: "Jake",
        nationalité: "Américaine",
        dateDeNaissance: "28/10/1974"
      },
      genre: "Fantastique",
      note: "4"
    },
    {
      title: "La Reine des neiges 2",
      anneePublication: "2019",
      langue: "Anglais",
      urlPoster:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png",
      realisateur: {
        nom: "Lee",
        prenom: "Jennifer",
        nationalité: "Américaine",
        dateDeNaissance: "22/10/1971"
      },
      genre: "Animation",
      note: "2"
    },
    {
      title: "Kill Bill",
      anneePublication: "2019",
      langue: "Anglais",
      urlPoster:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png",
      realisateur: {
        nom: "Tarantino",
        prenom: "Quentin",
        nationalité: "Américaine",
        dateDeNaissance: "27/03/1963"
      },
      genre: "Action",
      note: "3"
    },
    {
      title: "Your Name.",
      anneePublication: "2016",
      langue: "Japonais",
      urlPoster:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png",
      realisateur: {
        nom: "Shinkai",
        prenom: "Makoto",
        nationalité: "Japonais",
        dateDeNaissance: "09/02/1973"
      },
      genre: "Animation",
      note: "5"
    }
  ],
  genresFilm: [
    "Drame",
    "Comédie",
    "Thriller",
    "Action",
    "Fantastique",
    "Horreur",
    "Animation"
  ]
};

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
